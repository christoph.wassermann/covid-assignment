export interface CovidData {
  Global: CovidNumbers,
  Countries: CovidCountryData[]
}

export interface CovidNumbers {
  NewConfirmed: number;
  TotalConfirmed: number;
  NewDeaths: number;
  TotalDeaths: number;
  NewRecovered: number;
  TotalRecovered: number;
}

export interface CovidCountryData extends CovidNumbers {
  Country: string;
  CountryCode: string;
  Slug: string;
  Date: string;
}
