import { Component, Prop, h, Element, Event, EventEmitter } from '@stencil/core';
import { select } from 'd3';
import { isNil } from '../../global/util';

@Component({
  tag: 'dataviz-table',
  styleUrl: 'dataviz-table.css',
  shadow: true
})
export class DatavizTable {
  @Element() element;

  @Prop() headers: string[] = [];
  @Prop() data: (string|number)[][] = [];
  @Prop() rowBatchSize?: number;

  @Event() selectionChanged: EventEmitter<{index: number, rowData: (string|number)[]}>;

  numRowsDisplayed?: number;
  selectedIndex?: number;

  private updateRows() {
    const tbody = select(this.element.shadowRoot.querySelector('tbody'));
    const rows = tbody.selectAll('tr')
      .data(this.data.slice(0, this.numRowsDisplayed));

    const columnsRenderer = (d) => {
      let columnsHtml = '';
      d.forEach(item => {
        columnsHtml += `<td>${item}</td>`;
      })
      return columnsHtml;
    };

    const isSelected = (_d, i) => i === this.selectedIndex;

    rows.classed('selected', isSelected);

    rows.enter()
      .append('tr')
      .classed('selected', isSelected)
      .html(columnsRenderer)
      .on('click', this.setSelection.bind(this));

    rows.exit()
      .remove();

    this.updateMoreRowsLink();
  }

  private setSelection(rowData: (string|number)[], index: number) {
    this.selectedIndex = index;
    this.selectionChanged.emit({ rowData, index });
    this.updateRows();
  }

  private updateMoreRowsLink() {
    const moreRowsLink = select(this.element.shadowRoot.querySelector('.more-rows-link'));

    if (isNil(this.numRowsDisplayed) || this.numRowsDisplayed >= this.data.length) {
      // all rows already visible? -> no link at the bottom
      moreRowsLink.style('display', 'none');
    } else {
      // update text
      moreRowsLink.select('.info')
        .text(`(${this.data.length - this.numRowsDisplayed} more rows)`);
      moreRowsLink.style('display', 'block');
    }
  }

  componentWillLoad() {
    // set initial number of rows to defined batch size
    if (!isNil(this.rowBatchSize)) {
      this.numRowsDisplayed = this.rowBatchSize;
    }
  }

  componentDidLoad() {
    const moreRowsLink = select(this.element.shadowRoot.querySelector('.more-rows-link > .more'));
    moreRowsLink.on('click', () => {
      this.numRowsDisplayed += this.rowBatchSize || 1;
      this.updateRows();
    });

    this.updateRows();
  }

  render() {
    return (
      <div>
        <table>
          <thead>
            <tr>
              {this.headers.map(header =>
                <th>{header}</th>
              )}
            </tr>
          </thead>
          <tbody />
        </table>
        <div class="more-rows-link">
          <span class="info"></span>
          -
          <span class="more">Show More</span>
        </div>
      </div>
    );
  }
}
