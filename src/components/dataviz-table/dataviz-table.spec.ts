import { DatavizTable } from './dataviz-table';
import { newSpecPage } from '@stencil/core/testing';

describe('dataviz-table', () => {

  it('renders', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<dataviz-table></dataviz-table>'
    });
    expect(page.root.shadowRoot).toBeTruthy();
    expect(page.root.shadowRoot.querySelector('table')).toBeTruthy();
  });

  it('has a settable headers property', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<div></div>'
    });
    let component = page.doc.createElement('dataviz-table');
    (component as any).headers = ['Test header 1', 'Test header 2'];
    page.root.appendChild(component);
    await page.waitForChanges();

    expect(page.rootInstance.headers).toStrictEqual(['Test header 1', 'Test header 2']);
  });

  it('has a settable data property', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<div></div>'
    });
    let component = page.doc.createElement('dataviz-table');
    (component as any).data = [['Column 1', 1, 2, 3], ['Column 2', 4, 5, 6]];
    page.root.appendChild(component);
    await page.waitForChanges();

    expect(page.rootInstance.data).toStrictEqual([['Column 1', 1, 2, 3], ['Column 2', 4, 5, 6]]);
  });

  it('has a settable rowBatchSize property', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<div></div>'
    });
    let component = page.doc.createElement('dataviz-table');
    (component as any).rowBatchSize = 20;
    page.root.appendChild(component);
    await page.waitForChanges();

    expect(page.rootInstance.rowBatchSize).toBe(20);
  });

  it('reacts to the selection being set', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<dataviz-table></dataviz-table>'
    });
    const updateRowsMock = jest.fn();
    Object.defineProperty(page.rootInstance, 'updateRows', {
      value: updateRowsMock, writable: true
    });
    page.rootInstance.setSelection([], 4);
    expect(page.rootInstance.selectedIndex).toBe(4);
    expect(updateRowsMock).toHaveBeenCalled();
  });

  it('dispatches a selection event when selection has changed', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<dataviz-table></dataviz-table>'
    });
    const emitMock = jest.fn();
    Object.defineProperty(page.rootInstance.selectionChanged, 'emit', {
      value: emitMock, writable: true
    });
    page.rootInstance.setSelection([], 4);
    expect(emitMock).toHaveBeenCalledWith({rowData: [], index: 4});
  });

  it('updates the "more rows" link', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<dataviz-table></dataviz-table>'
    });
    page.rootInstance.numRowsDisplayed = 4;
    page.rootInstance.data = new Array(10);
    page.rootInstance.updateMoreRowsLink();
    expect(page.root.shadowRoot.querySelector('.more-rows-link .info')).toEqualHtml(`
      <span class="info">(6 more rows)</span>
    `);
  });

  it('reacts to a click on the "more rows" link', async () => {
    const page = await newSpecPage({
      components: [DatavizTable],
      html: '<dataviz-table></dataviz-table>'
    });
    const mockUpdateRows = jest.fn();
    Object.defineProperty(page.rootInstance, 'updateRows', {
      value: mockUpdateRows, writable: true
    });
    await page.waitForChanges();
    page.root.shadowRoot.querySelector('.more-rows-link > .more').dispatchEvent(new Event('click'));
    expect(mockUpdateRows).toHaveBeenCalled();
  });
});
