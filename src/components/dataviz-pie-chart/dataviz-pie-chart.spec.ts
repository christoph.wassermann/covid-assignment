import { newSpecPage } from '@stencil/core/testing';
import { DatavizPieChart } from './dataviz-pie-chart';

describe('dataviz-pie-chart', () => {

  it('renders', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<dataviz-pie-chart></dataviz-pie-chart>'
    });
    expect(page.root.shadowRoot).toBeTruthy();
    expect(page.root.shadowRoot.querySelector('svg')).toBeTruthy();
  });

  it('has a slices property', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<div></div>'
    });
    let component = page.doc.createElement('dataviz-pie-chart');
    (component as any).slices = {'test1': 1, 'test2': 2};
    page.root.appendChild(component);
    await page.waitForChanges();

    expect(page.rootInstance.slices).toStrictEqual({'test1': 1, 'test2': 2});
  });

  it('has a padding property', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<div></div>'
    });
    let component = page.doc.createElement('dataviz-pie-chart');
    (component as any).padding = 12;
    page.root.appendChild(component);
    await page.waitForChanges();

    expect(page.rootInstance.padding).toBe(12);
  });

  it('reacts to a window resize event', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<dataviz-pie-chart></dataviz-pie-chart>'
    });
    const resizeMock = jest.fn();
    Object.defineProperty(page.rootInstance, 'resizeComponentIfNecessary', {
      value: resizeMock, writable: true
    });
    page.rootInstance.componentDidLoad();
    page.win.dispatchEvent(new Event('resize'));
    expect(resizeMock).toHaveBeenCalled();
  });

  it('reacts to a window resize event with a different window size', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<dataviz-pie-chart></dataviz-pie-chart>'
    });
    page.rootInstance.dimensions.height = Infinity;
    page.rootInstance.dimensions.width = Infinity;
    page.win.dispatchEvent(new Event('resize'));
    expect(page.rootInstance.dimensions.height).not.toBe(Infinity);
    expect(page.rootInstance.dimensions.width).not.toBe(Infinity);
  });

  it('removes resize listener when disconnected', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<dataviz-pie-chart></dataviz-pie-chart>'
    });
    page.rootInstance.disconnectedCallback();

    expect(page.rootInstance.resizeListener).toBeFalsy();
  });

  it('completes when disconnected with no resize listener', async() => {
    const page = await newSpecPage({
      components: [DatavizPieChart],
      html: '<dataviz-pie-chart></dataviz-pie-chart>'
    });
    page.rootInstance.resizeListener = null;

    let exception: any;
    try {
      page.rootInstance.disconnectedCallback();
    } catch (e) {
      exception = e;
    }
    expect(exception).toBeUndefined();
  });

});
