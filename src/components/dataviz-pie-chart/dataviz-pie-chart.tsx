import {Component, Prop, h, Element, Watch} from '@stencil/core';
import { select, pie, entries, arc, PieArcDatum } from 'd3';

@Component({
  tag: 'dataviz-pie-chart',
  styleUrl: 'dataviz-pie-chart.css',
  shadow: true
})
export class DatavizPieChart {
  @Element() element: HTMLElement;

  @Prop() slices: {[key: string]: number} = {};
  @Prop() padding: number = 6;

  private dimensions = { width: 1, height: 1 };
  private resizeDebounceTimeout: NodeJS.Timeout;
  private resizeListener?: EventListenerObject;

  componentDidLoad() {
    this.initChart();
    this.resizeListener = this.resizeComponentIfNecessary.bind(this);
    window.addEventListener('resize', this.resizeListener);
  }

  disconnectedCallback() {
    if (this.resizeListener) {
      window.removeEventListener('resize', this.resizeListener);
      this.resizeListener = null;
    }
  }

  initChart() {
    const svg = select(this.element.shadowRoot.querySelector('svg'));
    this.dimensions.width = svg.node().clientWidth;
    this.dimensions.height = svg.node().clientHeight;
    svg.html('');
    svg.append('g');
    this.paintChart();
  }

  resizeComponentIfNecessary() {
    const svg = select(this.element.shadowRoot.querySelector('svg'));
    if (this.dimensions.height === svg.node().clientHeight
      && this.dimensions.width === svg.node().clientWidth) {
      return;
    }
    this.dimensions.height = svg.node().clientHeight || 0;
    this.dimensions.width = svg.node().clientWidth || 0;
    clearTimeout(this.resizeDebounceTimeout);
    this.resizeDebounceTimeout = setTimeout(this.paintChart.bind(this), 200);
  }

  @Watch('slices')
  paintChart() {
    const g = select(this.element.shadowRoot.querySelector('svg g'));
    const radius = Math.min(this.dimensions.width, this.dimensions.height) * .5 - this.padding;
    // const data = { 'Slice 1': 2, 'Slice 2': 4, 'Slice 3': 3};
    g.attr('transform', `translate(${this.dimensions.width * .5}, ${this.dimensions.height * .5})`);
    const pieGenerator = pie<{key: string, value: number}>().value(d => d.value);
    const pathGenerator = arc<PieArcDatum<{key: string, value: number}>>().outerRadius(radius).innerRadius(0);
    const paths = g.selectAll('path')
      .data(pieGenerator(entries(this.slices)));

    // update existing slices
    paths
      .attr('d', pathGenerator);

    // create new slices
    paths.enter()
      .append('path')
      .attr('d', pathGenerator)
      .attr('class', (_d, i) => `slice-${i}`);

    // remove unneeded slices
    paths.exit().remove();
  }

  render() {
    return (
      <svg />
    );
  }
}
