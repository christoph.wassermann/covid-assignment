import { newE2EPage } from '@stencil/core/testing';

describe('dataviz-root', () => {
  it('renders', async () => {
    const page = await newE2EPage({ url: '/'});

    const element = await page.find('dataviz-root');
    expect(element).toHaveClass('hydrated');
  });

  it('renders the title', async () => {
    const page = await newE2EPage({ url: '/'});

    const element = await page.find('dataviz-root >>> h1');
    expect(element.textContent).toEqual('COVID19 Summary');
  });
});
