jest.mock('d3');

import {newSpecPage} from "@stencil/core/testing";
import {DatavizRoot} from "./dataviz-root";
import {json} from 'd3';

describe('dataviz-root', () => {

  it('renders', async() => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });
    expect(page.root.shadowRoot).toBeTruthy();
    expect(page.root.shadowRoot.querySelector('.root-container > main')).toBeTruthy();
  });

  it('renders an error message if the data request fails', async () => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });
    page.rootInstance.errorMessage = 'error';
    expect(page.root.shadowRoot.querySelector('.root-container > main.error')).toBeTruthy();
  });

  it('renders the dataviz components if the data request succeeds', async () => {
    (json as jest.Mock).mockResolvedValue({ Global: {}, Countries: [] });

    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });
    page.rootInstance.errorMessage = '';
    // console.log(page.root.shadowRoot.querySelector('main').innerHTML);
    expect(page.root.shadowRoot.querySelector('dataviz-table')).toBeTruthy();
    expect(page.root.shadowRoot.querySelector('dataviz-pie-chart')).toBeTruthy();
  });

  it('prepares table data correctly', async () => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });

    page.rootInstance.rawData.Countries = [{
      Country: 'Country 1',
      CountryCode: 'C1',
      Slug: '',
      TotalConfirmed: 1,
      TotalDeaths: 2,
      TotalRecovered: 3,
      NewConfirmed: 0,
      NewDeaths: 0,
      NewRecovered: 0,
      Date: ''
    }, {
      Country: 'Country 2',
      CountryCode: 'C2',
      Slug: '',
      TotalConfirmed: 4,
      TotalDeaths: 5,
      TotalRecovered: 6,
      NewConfirmed: 0,
      NewDeaths: 0,
      NewRecovered: 0,
      Date: ''
    }];
    page.rootInstance.prepareData();
    expect(page.rootInstance.tableData).toStrictEqual([['Country 1', 1, 2, 3], ['Country 2', 4, 5, 6]]);
  });

  it('reacts when the data selection has been changed', async () => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });

    const mockSetDataSelection = jest.fn();
    Object.defineProperty(page.rootInstance, 'setDataSelection', {
      value: mockSetDataSelection, writable: true
    });
    page.root.dispatchEvent(new Event('selectionChanged'));
    expect(mockSetDataSelection).toHaveBeenCalled();
  });

  it('uses global data if the requested data selection is empty', async () => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });

    page.rootInstance.rawData = { Global: 'Test' };

    page.root.dispatchEvent(new CustomEvent<{index: number, rowData: (number|string)[]}>('selectionChanged',
      { detail: null }));
    expect(page.rootInstance.selection).toStrictEqual(page.rootInstance.rawData.Global);
  });

  it('uses country data if the requested data selection is not empty', async() => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });

    page.rootInstance.rawData = {
      Countries: [{
        Country: 'Country 1',
        CountryCode: 'C1',
        Slug: '',
        TotalConfirmed: 1,
        TotalDeaths: 2,
        TotalRecovered: 3,
        NewConfirmed: 0,
        NewDeaths: 0,
        NewRecovered: 0,
        Date: ''
      }, {
        Country: 'Country 2',
        CountryCode: 'C2',
        Slug: '',
        TotalConfirmed: 4,
        TotalDeaths: 5,
        TotalRecovered: 6,
        NewConfirmed: 0,
        NewDeaths: 0,
        NewRecovered: 0,
        Date: ''
      }]
    };

    page.root.dispatchEvent(new CustomEvent<{index: number, rowData: (number|string)[]}>('selectionChanged',
      { detail: { index: 1, rowData: ['Country 2', 4, 5, 6] }}));
    expect(page.rootInstance.selection).toStrictEqual(page.rootInstance.rawData.Countries[1]);
  });

  it('removes the data selection when the "clear" button is clicked', async () => {
    const page = await newSpecPage({
      components: [DatavizRoot],
      html: '<dataviz-root></dataviz-root>'
    });

    const mockSetDataSelection = jest.fn();
    Object.defineProperty(page.rootInstance, 'setDataSelection', {
      value: mockSetDataSelection, writable: true
    });
    page.root.shadowRoot.querySelector('.chart-container button').dispatchEvent(new Event('click'));
    expect(mockSetDataSelection).toHaveBeenCalledWith();
  });

});
