import { Component, h, Element, Listen } from '@stencil/core';
import { CovidData, CovidNumbers } from '../../global/types';
import { json } from 'd3';

const WEB_SERVICE_URL = 'https://api.covid19api.com/summary';
// const WEB_SERVICE_URL = 'assets/testData.json'

@Component({
  tag: 'dataviz-root',
  styleUrl: 'dataviz-root.css',
  shadow: true
})
export class DatavizRoot {
  @Element() element;

  errorMessage = '';
  rawData: CovidData;
  tableData: (string|number)[][];
  selection: CovidNumbers;

  private prepareData() {
    this.tableData = [];
    this.rawData.Countries.forEach(countryEntry => {
      this.tableData.push([
        countryEntry.Country,
        countryEntry.TotalConfirmed,
        countryEntry.TotalDeaths,
        countryEntry.TotalRecovered
      ]);
    });
    this.selection = this.rawData.Global;
  }

  @Listen('selectionChanged')
  private setDataSelection(e?: CustomEvent<{index: number, rowData: (number|string)[]}>) {
    if (!this.rawData) {
      return;
    }
    if (e && e.detail && e.detail.rowData && e.detail.rowData.length > 0) {
      this.selection = this.rawData.Countries.find(country => country.Country === e.detail.rowData[0]);
    } else {
      this.selection = this.rawData.Global;
    }
    const root = this.element.shadowRoot;
    root.querySelector('dataviz-pie-chart').slices = {
      'Total Active': this.selection.TotalConfirmed - this.selection.TotalDeaths - this.selection.TotalRecovered,
      'Total Deaths': this.selection.TotalDeaths,
      'Total Recovered': this.selection.TotalRecovered
    };
    root.querySelector('.chart-container h2').innerHTML = this.selection['Country'] || 'Global';
    root.querySelector('.chart-container .total-cases').innerHTML = this.selection.TotalConfirmed;
  }

  async componentWillLoad() {
    try {
      this.rawData = await json<CovidData>(WEB_SERVICE_URL);
      this.prepareData();
    } catch (e) {
      this.errorMessage = e.message;
      this.rawData = null;
    }
  }

  componentDidRender() {
    this.setDataSelection();
  }

  render() {
    return (
      <div class={"root-container"}>
        {this.errorMessage &&
          <main class="error">
            <p>An error has occurred while loading and preparing the latest Covid data
              from <a href={"WEB_SERVICE_URL"}>{WEB_SERVICE_URL}</a>. Here's the error message:</p>
            <p class="message">{this.errorMessage}</p>
          </main>
        }
        {!this.errorMessage &&
          <main>
            <h1>COVID19 Summary</h1>
            <p>This is an overview over some of the key statistical numbers on the COVID19 pandemic. The data
              is provided by the <a href="https://covid19api.com/">Covid19 API</a>. You can click on a country
              in the table to visualize its numbers in the pie chart to the right.</p>
            <dataviz-table
              headers={['Country', 'Total confirmed', 'Total deaths', 'Total recovered']}
              data={this.tableData}
              rowBatchSize={6}
            />
            <div class="chart-container">
              <h2>Global</h2>
              <div>Total confirmed cases: <span class="total-cases">{this.selection.TotalConfirmed || '?'}</span></div>
              <dataviz-pie-chart />
              <button onClick={() => this.setDataSelection()}>Clear Selected Country</button>
            </div>
          </main>
        }
        <footer>
          <p>CONTACT US</p>
          <div>
            <p>+44 1234 5678</p>
            <p>covid19@gmail.com</p>
          </div>
          <div>&copy; COVID19 Summary 2020</div>
        </footer>
      </div>
    );
  }
}
