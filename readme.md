# Covid19 data visualization exercise

Programmed using [TypeScript](https://www.typescriptlang.org) on 
[Node.js](https://nodejs.org), with [stencil.js](https://stenciljs.com) as the component toolchain 
and [D3.js](https://d3js.org) for visualization. 
For testing, the application uses [jest](https://jestjs.io/) and [puppeteer](https://github.com/puppeteer/puppeteer/). 

D3.js is the only non-development dependency.

### Live application

See the live application here:  
https://kamakodo.gitlab.io/covid-assignment/

The ``bonus`` branch live application can be accessed here:  
https://kamakodo.gitlab.io/-/covid-assignment/-/jobs/592139392/artifacts/public/index.html

### Building and testing

After cloning this repository, install the dependencies first: 

    $ npm install

You can run the application on localhost easily:

    $ npm start 

If you just need the build, run

    $ npm run build
    
Execute unit and e2e tests, and report test coverage:

    $ npm test

### Requirements fulfillment

The basic requirements have been fulfilled as follows:

- [x] The data table is populated live from https://api.covid19api.com/summary
- [x] The application uses [D3.js](https://d3js.org) to create both the table and the pie chart.  
- [x] The pie chart displays global numbers if nothing is selected, or the selected country's numbers if a country row 
        has been clicked on in the table.
- [x] Both the table and the pie chart are packaged as web components using [stencil.js](https://stenciljs.com/docs/introduction).
- [x] Theme colors are defined as CSS variables in global/app.css. These can be easily changed, and control the whole 
        application's look.   
- [x] Each component has an attached .spec file containing unit tests, with 100% coverage (see ``npm test``).
- [x] For readability and reusability, the code was organized into encapsulated components.

Additionally, the bonus requirements have been partially fulfilled:

- [x] The application does not use any additional frontend frameworks like Bootstrap, Foundation, Bulma etc.
- [ ] The chart and table components do not have smooth transitions yet.
- [ ] The application does not yet support smaller screens / RWD
- [ ] There is only one POC e2e test so far, a lot more should be created.
- [ ] The look and feel could be worked on. 

The following features are implemented in this repository's ``bonus`` branch: 
- [x] Revised look and feel
- [x] Table can be searched for countries
- [x] Chart and table animations
- [x] Chart labels (can be manually toggled)
- [x] Responsiveness: App can be used on screens down to iPhone 6-ish sizes
- [x] An additional calculated table column: Active cases